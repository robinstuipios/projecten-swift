//
//  MinorKiezenViewController.swift
//  minorkeuze
//
//  Created by Robin Stuip on 18-09-14.
//  Copyright (c) 2014 Robin Stuip. All rights reserved.
//

import UIKit
import CoreData

class ChooseMinorViewController: UIViewController {

    var minorA : NSManagedObject? = nil
    var minorB : NSManagedObject? = nil
    
    @IBOutlet weak var minorAView: UIView!
    @IBOutlet weak var minorBView: UIView!
    
    @IBOutlet var minorACode: UILabel!
    @IBOutlet var minorAName: UILabel!

    @IBOutlet var minorBCode: UILabel!
    @IBOutlet var minorBName: UILabel!
    
    let fullRotation = CGFloat(M_PI * 2)
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.addSubview(minorAView)
        self.view.addSubview(minorBView)
        self.setChosableMinors()
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    func setChosableMinors()
    {
        if(minorMgr.minors.count-2 >= minorMgr.previousMinors.count){
            for minor in minorMgr.minors
            {
                if(minorMgr.checkInPreMinor(minor) && minorA == nil)
                {
                    minorA = minor
                    minorMgr.addPreviousMinor(minor)
                    minorACode.text = minor.valueForKey("code") as String!
                    minorAName.text = minor.valueForKey("name") as String!
                }
                else if (minorMgr.checkInPreMinor(minor) && minorB == nil)
                {
                    minorB = minor
                    minorMgr.addPreviousMinor(minor)
                    minorBCode.text = minor.valueForKey("code") as String!
                    minorBName.text = minor.valueForKey("name") as String!
                }
            }
        }
        else{
            
        }
    }

    func animateViewBlock(minorAClicked : Bool)
    {
        var clickedView : UIView
        var unclickedView : UIView!
        if(minorAClicked == true)
        {
            clickedView = minorAView
            unclickedView = minorBView
        }
        else
        {
            clickedView = minorBView
            unclickedView = minorAView
        }
        var initialX = unclickedView.frame.origin.x;
        var initialY = unclickedView.frame.origin.y;
        var initialWidth = unclickedView.bounds.width
        var initialHeight = unclickedView.bounds.height
        var horizontalWidth = view.frame.maxX
        if(minorMgr.minors.count-2 <= minorMgr.previousMinors.count){
            UIView.animateWithDuration(2, animations: {
                unclickedView.alpha = 0.0
                unclickedView.alpha = 0.0
                unclickedView.alpha = 0.0
                clickedView.frame = CGRect(x: 20, y: initialHeight, width: horizontalWidth-40, height: initialHeight)
            }, completion: {finished in
                UIView.animateWithDuration(2.0, animations: {
                clickedView.backgroundColor = UIColor.greenColor()
                clickedView.alpha = 0.0
                clickedView.alpha = 1.0
                //self.navigationController?.popToRootViewControllerAnimated(true)
                //minorMgr.previousMinors.removeAll(keepCapacity: false)
                })
                })
        }
        else
        {
        
        let options = UIViewKeyframeAnimationOptions.CalculationModeLinear
        let optionsSlide = UIViewAnimationOptions.TransitionNone
        
        UIView.animateWithDuration(1.0, animations: {
            unclickedView.alpha = 0.0
            unclickedView.alpha = 0.0
            unclickedView.alpha = 0.0
        })
        UIView.animateWithDuration(1.5, delay: 0.0, options: optionsSlide, animations: {
            
            unclickedView.frame = CGRect(x: initialX, y: -100, width: initialWidth, height: initialHeight)
            
            }, completion: { finished in
        
            
        })
        UIView.animateKeyframesWithDuration(1.0, delay: 0.0, options: options, animations: {
            UIView.addKeyframeWithRelativeStartTime(0, relativeDuration: 1/3, animations: {
                clickedView.transform = CGAffineTransformMakeRotation(1/3 * self.fullRotation)
                CGRect(x: 0, y: 0, width: 200, height: 200)
            } )
            UIView.addKeyframeWithRelativeStartTime(1/3, relativeDuration: 1/3, animations: {
                clickedView.transform = CGAffineTransformMakeRotation(2/3 * self.fullRotation)
            })
            UIView.addKeyframeWithRelativeStartTime(2/3, relativeDuration: 1/3, animations: {
                clickedView.transform = CGAffineTransformMakeRotation(3/3 * self.fullRotation)
            })
            }, completion: {finished in
                
                UIView.animateWithDuration(1.0, animations: {
                self.setChosableMinors()
                    unclickedView.alpha = 1.0
                    unclickedView.alpha = 1.0
                    unclickedView.alpha = 1.0
                unclickedView.frame = CGRect(x: initialX, y: initialY, width: initialWidth, height: initialHeight)
                })
        })
}
    }
    @IBAction func btnChooseMinorA_Click(sender: UIButton){
        minorMgr.deletePreviousMinor(minorA!)
        minorMgr.setSelectedMinor(minorA!)
        minorA = nil
        minorB = nil
        animateViewBlock(true);
    }
    @IBAction func btnChooseMinorB_Click(sender: UIButton){
        minorMgr.deletePreviousMinor(minorB!)
        minorMgr.setSelectedMinor(minorB!)
        minorA = nil
        minorB = nil
        animateViewBlock(false);
    }
}

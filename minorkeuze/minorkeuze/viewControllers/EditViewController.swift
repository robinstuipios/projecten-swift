//
//  EditViewController.swift
//  minorkeuze
//
//  Created by Robin Stuip on 17-09-14.
//  Copyright (c) 2014 Robin Stuip. All rights reserved.
//

import UIKit

class EditViewController: UIViewController, UITextFieldDelegate {
    
    
    @IBOutlet var txtCode: UITextField!
    @IBOutlet var txtName: UITextField!
    
    var id: Int!
    var code : NSString!
    var name : NSString!
    var date : String?
    
    @IBOutlet weak var datepicker: UIDatePicker!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setValues(self.code, name: self.name, date: self.date!)
        dataChanged(self)
        // Do any additional setup after loading the view.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    func setValues(code : NSString, name : NSString, date : String)
    {
        txtCode.text = code
        txtName.text = name
        var dateFormatter = NSDateFormatter()
        dateFormatter.dateFormat = "MM-dd-yyyy HH:mm:ss"
        dateFormatter.dateFromString(date)
        datepicker.setDate(dateFormatter.dateFromString(date)!, animated: true)
    }
    @IBAction func dataChanged(sender: AnyObject) {
        var dateFormatter = NSDateFormatter()
        
        dateFormatter.dateFormat = "MM-dd-yyyy HH:mm:ss"
        var strDate = dateFormatter.stringFromDate(datepicker.date)
        date = strDate
    }

    @IBAction func btnEditMinor_Click(sender: UIButton){
        var alert = UIAlertController(title: "Alert", message: "Fill in all the fields", preferredStyle: UIAlertControllerStyle.Alert)
        alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.Default, handler: nil))
        if(txtCode.text.isEmpty || txtName.text.isEmpty){
            self.presentViewController(alert, animated: true, completion: nil)
            self.view.endEditing(true)
        }
        else{
            minorMgr.editMinor(id, code: txtCode.text, name: txtName.text, date: date!);
            self.view.endEditing(true)
            txtCode.text = ""
            txtName.text = ""
            self.tabBarController?.selectedIndex = 0
            self.navigationController?.popToRootViewControllerAnimated(true)
        }
    }
}

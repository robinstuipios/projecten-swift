//
//  ViewController.swift
//  minorkeuze
//
//  Created by Robin Stuip on 17-09-14.
//  Copyright (c) 2014 Robin Stuip. All rights reserved.
//

import UIKit
import CoreData

class ShowMinorsViewController: UIViewController, UITableViewDelegate{ //UITableViewDataSource {

    @IBOutlet var tblMinors: UITableView!
    var selectedRow : Int!
    
    override func viewDidLoad() {
        minorMgr.loadMinors()
        super.viewDidLoad()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    override func viewWillAppear(animated: Bool) {
        if(tblMinors != nil){
            tblMinors.reloadData()
        }
    }
    
    func tableView(tableView: UITableView, commitEditingStyle editingStyle: UITableViewCellEditingStyle, forRowAtIndexPath indexPath: NSIndexPath){
        if(editingStyle == UITableViewCellEditingStyle.Delete){
            minorMgr.deleteMinor(indexPath.row)
            tblMinors.reloadData()
        }
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int{
        return minorMgr.minors.count
    }
    
    func tableView(tableView: UITableView!, cellForRowAtIndexPath indexPath: NSIndexPath!) -> UITableViewCell! {
        
        var cell = tableView.dequeueReusableCellWithIdentifier("minorcell", forIndexPath: indexPath) as? UITableViewCell

        cell?.textLabel?.text = minorMgr.minors[indexPath.row].valueForKey("code") as String!
        cell?.detailTextLabel?.text = minorMgr.minors[indexPath.row].valueForKey("name") as String!
        
        if(minorMgr.minors[indexPath.row].valueForKey("code") as String! == minorMgr.selectedMinor?.code){
            cell?.backgroundColor = UIColor(red: 0, green: 122.0/255.0, blue: 1.0, alpha: 1.0)
        }
        return cell
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath)
    {
        self.selectedRow = indexPath.row
        self.performSegueWithIdentifier("edit", sender: self)
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue?, sender: AnyObject!) {
        
        if segue!.identifier == "edit" {

            let editViewController:EditViewController = segue!.destinationViewController as EditViewController
            editViewController.id = selectedRow
            editViewController.code = minorMgr.minors[selectedRow].valueForKey("code") as String
            editViewController.name = minorMgr.minors[selectedRow].valueForKey("name") as String
            editViewController.date = minorMgr.minors[selectedRow].valueForKey("date") as? String
        }
    }
}

/*
// MARK: - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
override func prepareForSegue(segue: UIStoryboardSegue!, sender: AnyObject!) {
// Get the new view controller using segue.destinationViewController.
// Pass the selected object to the new view controller.
}
*/


//
//  AddViewController.swift
//  minorkeuze
//
//  Created by Robin Stuip on 17-09-14.
//  Copyright (c) 2014 Robin Stuip. All rights reserved.
//

import UIKit
import CoreData

class AddViewController: UIViewController, UITextFieldDelegate {

    @IBOutlet var txtCode: UITextField!
    @IBOutlet var txtName: UITextField!
    var date : NSString = ""
    
    @IBOutlet weak var uiterlijkeInschrijfDatum: UIDatePicker!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        dateChanged(self);
        // Do any additional setup after loading the view, typically from a nib.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    @IBAction func dateChanged(sender: AnyObject) {
        var dateFormatter = NSDateFormatter()
        
        dateFormatter.dateFormat = "MM-dd-yyyy HH:mm:ss";
        var strDate = dateFormatter.stringFromDate(uiterlijkeInschrijfDatum.date)
        date = strDate
    }
    @IBAction func btnAddMinor_Click(sender: UIButton){
        var alert = UIAlertController(title: "Alert", message: "Fill in all the fields", preferredStyle: UIAlertControllerStyle.Alert)
        alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.Default, handler: nil))
        
        if(txtCode.text.isEmpty || txtName.text.isEmpty){
            self.presentViewController(alert, animated: true, completion: nil)
            self.view.endEditing(true)
        }
        else{
            minorMgr.addMinor(txtCode.text, name: txtName.text, date: date);
            self.view.endEditing(true)
            txtCode.text = ""
            txtName.text = ""
            self.tabBarController?.selectedIndex = 0;
            self.navigationController?.popToRootViewControllerAnimated(true)
        }
    }
    
    override func touchesBegan(touches: NSSet, withEvent event: UIEvent) {
        self.view.endEditing(true)
    }
    
    //UITextField Delegate
    func textFieldShouldReturn(textField: UITextField) -> Bool{
        textField.resignFirstResponder()
        return true;
    }
}

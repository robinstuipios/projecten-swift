//
//  InschrijvingViewController.swift
//  minorkeuze
//
//  Created by Robin Stuip on 25-09-14.
//  Copyright (c) 2014 Robin Stuip. All rights reserved.
//

import UIKit

class EnrollmentViewController: UIViewController, UIPickerViewDelegate {

    @IBOutlet var userName: UITextField!
    @IBOutlet var password: UITextField!
    @IBOutlet var year: UITextField!
    @IBOutlet weak var minor: UIPickerView!

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    // returns the number of 'columns' to display.
    func numberOfComponentsInPickerView(pickerView: UIPickerView) -> Int{
        return 1
    }
    
    // returns the # of rows in each component..
    func pickerView(pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int{
        return minorMgr.minors.count
    }
    
    func pickerView(pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String!{
        return minorMgr.minors[row].valueForKey("code") as String
    }
    

    @IBAction func btnEnrollMinor_Click(){
        /*println(userName.text)
        println(password.text)
        println(year.text)
        println(minor.dataSource)*/
        studentMgr.studentEnrollForMinor(userName.text, password: password.text);
    }

    //UITextField Delegate
    func textFieldShouldReturn(textField: UITextField) -> Bool{
        textField.resignFirstResponder()
        return true;
    }
}

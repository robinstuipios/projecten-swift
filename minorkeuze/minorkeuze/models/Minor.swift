//
//  Minors.swift
//  minorkeuze
//
//  Created by Robin Stuip on 30-09-14.
//  Copyright (c) 2014 Robin Stuip. All rights reserved.
//

import Foundation
import CoreData

@objc(Minor)
class Minor: NSManagedObject {

    @NSManaged var code: String
    @NSManaged var date: NSDate
    @NSManaged var name: String

}

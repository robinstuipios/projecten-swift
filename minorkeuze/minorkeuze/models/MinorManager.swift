//
//  MinorManager.swift
//  minorkeuze
//
//  Created by Robin Stuip on 17-09-14.
//  Copyright (c) 2014 Robin Stuip. All rights reserved.
//

import CoreData
import UIKit

var minorMgr: MinorManager = MinorManager()

class MinorManager: NSObject {
    
    var minors = [NSManagedObject]()
    var selectedMinor : Minor?
    var previousMinors = [NSManagedObject]()
    let appDel:AppDelegate = (UIApplication.sharedApplication().delegate as AppDelegate)
    
    // minors
    func loadMinors()
    {
        minors.removeAll(keepCapacity: false)
        var context:NSManagedObjectContext = appDel.managedObjectContext!
        var request = NSFetchRequest(entityName: "Minors")
        request.returnsObjectsAsFaults = false
        var results:NSArray = context.executeFetchRequest(request, error: nil)!
        
        if(results.count > 0)
        {
            for var index = 0; index < results.count; ++index
            {
                var res = results[index] as NSManagedObject
                minors.append(res)
            }
        }
    }
    func addMinor(code: String, name: String, date : String){
        var context:NSManagedObjectContext = appDel.managedObjectContext!
        var newMinor = NSEntityDescription.insertNewObjectForEntityForName("Minors", inManagedObjectContext: context) as NSManagedObject
        
        newMinor.setValue(code, forKey: "code")
        newMinor.setValue(name, forKey: "name")
        newMinor.setValue(date, forKey: "date")
        
        context.save(nil)
        self.loadMinors()
    }

    func editMinor(id : Int, code: String, name: String, date: String){
        var context:NSManagedObjectContext = appDel.managedObjectContext!
        minors[id].setValue(code, forKey: "code")
        minors[id].setValue(name, forKey: "name")
        minors[id].setValue(date, forKey: "date")
        context.save(nil)
        self.loadMinors()
    }
    
    func deleteMinor(id : Int){
        var context:NSManagedObjectContext = appDel.managedObjectContext!
        context.deleteObject(minors[id])
        context.save(nil);
        self.loadMinors()
    }
    
    // previousminors
    func addPreviousMinor(preMinor : NSManagedObject){
        previousMinors.append(preMinor)
    }
    func deletePreviousMinor(preMinor: NSManagedObject){
        for var index = 0; index < self.previousMinors.count; ++index{
            if(previousMinors[index].valueForKey("code") as String! == preMinor.valueForKey("code") as String!){
                previousMinors.removeAtIndex(index)
            }
        }
    }
    func checkInPreMinor(checkMinor: NSManagedObject) -> Bool{
        var returnValue = true
        for isminorinpreviousminors in previousMinors
        {
            if(checkMinor.valueForKey("code") as String! == isminorinpreviousminors.valueForKey("code") as String!)
            {
                returnValue = false
            }
        }
        return returnValue
    }
    
    // selectedminor
    func setSelectedMinor(selectedMinor : NSManagedObject){
        self.selectedMinor = (selectedMinor as Minor)
    }
    
    // other
    func notificationsActivated()
    {
        for minor in minors
        {
            var dateFormatter = NSDateFormatter()
            let notification: UILocalNotification = UILocalNotification()
            notification.timeZone = NSTimeZone.defaultTimeZone()
            var code: AnyObject? = minor.valueForKey("code")
            var date = minor.valueForKey("date") as String
            let dateTime = NSDate()
            dateFormatter.dateFormat = "MM-dd-yyyy HH:mm:ss"
            notification.fireDate = dateFormatter.dateFromString(date)
            notification.alertBody = "Inschrijven voor \(code) verlopen!"
            UIApplication.sharedApplication().scheduleLocalNotification(notification)
        }
    }
    
}

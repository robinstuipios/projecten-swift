//
//  StudentManager.swift
//  minorkeuze
//
//  Created by Robin Stuip on 30-09-14.
//  Copyright (c) 2014 Robin Stuip. All rights reserved.
//

import UIKit

var studentMgr: StudentManager = StudentManager()

class StudentManager: NSObject {
    func studentEnrollForMinor(userName : String, password : String)
    {
        var paths = NSSearchPathForDirectoriesInDomains(.DocumentDirectory, .UserDomainMask, true)[0] as String
        var path = paths.stringByAppendingPathComponent("userinformation.plist")
        var fileManager = NSFileManager.defaultManager()
        
        if (!(fileManager.fileExistsAtPath(path)))
        {
            var bundle : NSString = NSBundle.mainBundle().pathForResource("data", ofType: "plist")!
            fileManager.copyItemAtPath(bundle, toPath: path, error:nil)
        }
        let data = "Username: "+userName+"\n"+"Password:"+password
        data.writeToFile(path, atomically: true, encoding: NSUTF8StringEncoding, error: nil)
    }
}

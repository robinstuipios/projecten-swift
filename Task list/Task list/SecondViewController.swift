//
//  SecondViewController.swift
//  Task list
//
//  Created by Robin Stuip on 03-09-14.
//  Copyright (c) 2014 Robin Stuip. All rights reserved.
//

import UIKit

class SecondViewController: UIViewController, UITextFieldDelegate{
    
    @IBOutlet var txtTask: UITextField!
    @IBOutlet var txtDesc: UITextField!
    @IBOutlet var completed: UISwitch!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    @IBAction func btnAddTask_Click(sender: UIButton){
        var alert = UIAlertController(title: "Alert", message: "Fill in all the fields", preferredStyle: UIAlertControllerStyle.Alert)
        alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.Default, handler: nil))
        
        if(txtTask.text.isEmpty || txtDesc.text.isEmpty){
            self.presentViewController(alert, animated: true, completion: nil)
            self.view.endEditing(true)
        }
        else{
            taskMgr.addTask(txtTask.text, desc: txtDesc.text, completed: completed.on);
            println(completed.on)
            self.view.endEditing(true)
            txtTask.text = ""
            txtDesc.text = ""
            completed.setOn(false, animated: true)
            self.tabBarController?.selectedIndex = 0;
        }
    }
    
    override func touchesBegan(touches: NSSet, withEvent event: UIEvent) {
        self.view.endEditing(true)
    }
    
    //UITextField Delegate
    func textFieldShouldReturn(textField: UITextField) -> Bool{
        textField.resignFirstResponder();
        return true;
    }
}


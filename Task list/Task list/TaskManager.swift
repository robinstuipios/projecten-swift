//
//  TaskManager.swift
//  Task list
//
//  Created by Robin Stuip on 03-09-14.
//  Copyright (c) 2014 Robin Stuip. All rights reserved.
//

import UIKit

var taskMgr: TaskManager = TaskManager()

struct task{
    var name = "Un-named"
    var desc = "Un-decribed"
    var completed = false
}

class TaskManager: NSObject {
   
    var tasks = [task]()
    
    func addTask(name: String, desc: String, completed: Bool){
        tasks.append(task(name: name, desc: desc, completed: completed))
    }
}

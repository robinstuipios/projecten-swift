//
//  APIConnector.swift
//  API-key
//
//  Created by Robin Stuip on 10-09-14.
//  Copyright (c) 2014 Robin Stuip. All rights reserved.
//

import UIKit
import Foundation

class APIConnector: NSObject {
     dynamic var responseString = NSString()
    
    func sendRequest(data: String, url: String)
    {
        var URL: NSURL = NSURL(string: "http://toolforthemind.com/api")
        var request:NSMutableURLRequest = NSMutableURLRequest(URL:URL)
        request.HTTPMethod = "POST"
        var bodyData = "apikey=tftmr0x"
        request.HTTPBody = bodyData.dataUsingEncoding(NSUTF8StringEncoding);

        NSURLConnection.sendAsynchronousRequest(request, queue: NSOperationQueue.mainQueue(), completionHandler:{ (response: NSURLResponse!, data: NSData!, error: NSError!) -> Void in
            
            if error != nil{
                println("error");
            }
            else{
                //println(NSString(data: data, encoding: NSUTF8StringEncoding))
                var responseStr:NSString = NSString(data: data, encoding: NSUTF8StringEncoding)
                //println(responseStr);
                self.responseString = responseStr
                
            }
        })
        
        //println(responseString)
        //return responseString
        
    }
}

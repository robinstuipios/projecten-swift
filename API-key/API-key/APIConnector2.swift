//
//  APIConnector2.swift
//  API-key
//
//  Created by Robin Stuip on 12-09-14.
//  Copyright (c) 2014 Robin Stuip. All rights reserved.
//

import UIKit

class APIConnector2: NSObject {
    var data = NSMutableData()
    
    func callAsynchronous(urlString:String)
    {
        NSLog("connectWithUrl")
        var url = NSURL.URLWithString(urlString)// Creating URL
        var request:NSMutableURLRequest = NSMutableURLRequest(URL: url)// Creating Http Request
        request.HTTPMethod = "POST"
        var bodyData = "apikey=tftmr0x"
        request.HTTPBody = bodyData.dataUsingEncoding(NSUTF8StringEncoding);
        //Making request
        var connection = NSURLConnection(request: request, delegate: self, startImmediately: true)
    }
    
    func connection(connection: NSURLConnection!, didReceiveResponse response: NSURLResponse!)
    {
        //Will be called when
        NSLog("didReceiveResponse")
    }
    
    func connection(connection: NSURLConnection!, didReceiveData _data: NSData!)
    {
        NSLog("didReceiveData")
        self.data.appendData(_data)
    }
    
    func connectionDidFinishLoading(connection: NSURLConnection!) -> NSString
    {
        NSLog("connectionDidFinishLoading")
        
        var responseStr:NSString = NSString(data:self.data, encoding:NSUTF8StringEncoding)
        println(responseStr);
        return responseStr
    }
    
    func connection(connection: NSURLConnection!, didFailWithError error: NSError!)
    {
        NSLog("didFailWithError=%@",error)
        println(NSLog)
    }

}

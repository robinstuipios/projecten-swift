//
//  ViewController.swift
//  API-key
//
//  Created by Robin Stuip on 10-09-14.
//  Copyright (c) 2014 Robin Stuip. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    
    var apiConnector: APIConnector = APIConnector();
    var apiConnector2: APIConnector2 = APIConnector2();
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.addObserver(self.apiConnector, forKeyPath: "responseString", options: NSKeyValueObservingOptions.New, context: nil)
        
    }
    
    override func observeValueForKeyPath(keyPath: String!, ofObject object: AnyObject!, change: [NSObject : AnyObject]!, context: UnsafeMutablePointer<Void>) {
        println(self.apiConnector.responseString)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    @IBAction func sendRequest(sender: UIButton){
        var url = "http://toolforthemind.com/api/";
        var keydata = "apikey=tftmr0x";
        
        //var message : NSString = apiConnetcor.sendRequest(keydata, url: url)
        apiConnector.sendRequest(keydata, url: url)
        
        
        
        
    }
    @IBAction func sendRequest2(sender: UIButton){
        apiConnector2.callAsynchronous("http://toolforthemind.com/api/")
    }
}

